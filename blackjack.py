# !/usr/bin/python
# -*-coding: utf-8-*-

# Got the rules in: https://www.blackjackapprenticeship.com/how-to-play-blackjack/
# Reglas en Castellano: https://www.casinosonlineespana.net/blackjack/reglas/

# BlackJack game, you have to reach 500 $ to win the IA, Good Luck!
# Game developed by Pau Martín in Python3. 
# USAGE: python3 blackjack.py

############### Import Libraries #############

from random import randint
from time import sleep 
import os

############## Rules #####################

def mostra_instruccions(language):
	
	# Show the rules and objectives in the specified language.
	# IN: STR as the language select.
	# OUT: STR
	
	if (language == "eng"):
		
		os.system('clear')
		print ("Course and objective of the game:")
		print ("Before the start of each game, that is, before the cards are dealt, the participating players must place their bets, respecting the limits of the table. Once this is done, the dealer deals the cards and each player receives two cards that are placed on the table face up. The dealer also has two cards but one of them is face down. ")
		print ("The player to the left of the dealer starts the game first. The options are as follows: you can request another card (Ask), divide the two cards in two hands and ask for cards for both hands (Divide), you can end your turn (Stand). When all players are ready, the dealer shows his second card. The one with 21 points or less wins and the one with more than 21 points loses. ")
		print ("If the hand card score is less than 17 you can ask for a card and if you have 17 you must plant because the risk of exceeding 21 with another card is much higher. You have to take into account that the Ace It is counted as 1 point and 11 points according to the player's desire to combine the cards in his hand for the optimal score of 21. The goal is to have a score of 21 at the end of the game or be closer to 21 than the dealer. If the dealer and the player are tied, then the bet money is recovered. ")
		print ()
		print ("User rules:")
		print ("The Ace may be worth 1 point or 11 points depending on the desire to combine it with the other card in the hand. Do not forget that the maximum winning score is 21 points.")
		print ("The numbers 2 to 10, equal to their own value. For example if you have two cards of 8, the hand has a value of 16 points and it is advisable to divide the cards into two separate hands.")
		print ("Letters with figures (Jack, Lady, King) are worth 10 points each.")
		print ()
		print ("IA Rules:")
		print ("When all players have finished it comes the dealer's turn. The rule you must always follow is this:")
		print ("16 or less = request letter, 17 or more = stand")
		print ()
		print ("Terminology:")
		print ("Fold: placing an additional bet on the primitive, the dealer serves the player a single card on the two initials. Each casino has rules for this play; some always allow doubling, whatever the first two cards, others only allow doubling if the first two cards add 9, 10 or 11. ")
		print ("Separate: if the first two cards are of the same numerical value, the player can separate them, placing an additional bet. On each of the separate cards he is served one more, each game of two cards being an independent play. Yes the second card served on each play is equal to the first, you can separate that play again by placing another additional bet, and so on. ")
		print ("Secure / secure: play that consists of betting that the dealer will get Blackjack, when his first card is an Ace. You cannot bet on this move over half of the initial bet. If the dealer gets Blackjack pays to the player twice the insurance bet. ")
		print ()
			
	elif (language == "spa"):
	
		os.system('clear')
		print ("Transcurso y objetivo del juego:")
		print ("Antes del inicio de cada juego, o sea antes del reparto de las cartas, los jugadores que participan deben realizar sus apuestas, respetando los límites de la mesa. Una vez hecho esto, el crupier reparte las cartas y cada jugador recibe dos cartas que se ponen en la mesa boca arriba. El crupier tiene también dos cartas pero una de ellas está boca abajo.")
		print ("Primero empieza el juego el jugador que está a la izquierda del crupier. Las opciones son las siguientes: puede solicitar otra carta (Pedir), dividir en dos manos las dos cartas y pedir cartas para las dos manos (Dividir), puede finalizar su turno (Plantarse). Cuando todos los jugadores están listos, el crupier enseña su segunda carta. El que tiene 21 puntos o menos gana y el que tiene más de 21 puntos pierde.")
		print ("Si la puntuación de las cartas de la mano es menor de 17 se puede pedir carta y si se tiene 17 se debe plantar porque el riesgo de sobrepasar el 21 con otra carta es mucho mayor. Hay que tener en cuenta que el As se cuenta como 1 punto y 11 puntos según el deseo del jugador de combinar las cartas en la mano para la puntuación óptima de 21. El objetivo es de tener al final del juego una puntuación de 21 o ser más cerca del 21 que el crupier. Si el crupier y el jugador están empatados, entonces el dinero de la apuesta se recupera.")
		print ()
		print ("Reglas usuario: ")
		print ("El As puede valer 1 punto u 11 puntos según el deseo de combinarlo con la otra carta de la mano. No hay que olvidar que la máxima puntuación ganadora es de 21 puntos.")
		print ("Los números del 2 al 10, equivalen a su propio valor. Por ejemplo si tiene dos cartas de 8, la mano tiene valor de 16 puntos y es aconsejable dividir las cartas en dos manos separadas.")
		print ("Cartas con figuras (Jota, Dama, Rey) valen cada una 10 puntos.")
		print ()
		print ("Reglas IA: ")
		print ("Cuando todos los jugadores hayan terminado viene el turno del crupier. La regla que debe cumplir siempre es esta:")
		print ("16 o menos = pedir carta, 17 o más = plantarse")
		print ()
		print ("Terminología: ")
		print ("Doblar: poniendo una apuesta adicional a la primitiva, el crupier le sirve al jugador una sola carta sobre las dos iniciales. Cada casino tiene reglas para esta jugada; unos permiten doblar siempre, sean cuales sean las dos primeras cartas, otros sólo permiten doblar si las dos primeras cartas suman 9, 10 u 11.")
		print ("Separar: si las dos primeras cartas son del mismo valor numérico, el jugador puede separarlas, poniendo una apuesta adicional. Sobre cada una de las cartas separadas se le sirve una más, constituyendo cada juego de dos cartas una jugada independiente. Si la segunda carta servida sobre cada jugada es igual a la primera, puede volver a separar esa jugada poniendo otra apuesta adicional, y así sucesivamente.")
		print ("Asegurar/seguro: jugada que consiste en apostar a que el crupier obtendrá Blackjack, cuando su primera carta es un As. No se puede apostar en esta jugada por encima de la mitad de la apuesta inicial. Si el crupier obtiene Blackjack paga al jugador dos veces la apuesta del seguro.")
		print ()
		
	sortir_instruccions=input("Press a key to exit...")

def help_rules():
	
	# Show a menu where user can select the instructions language
	# OUT: STR
	
	os.system('clear')
	print ("Language rules?")
	print ()
	print ("1 --> ENGLISH")
	print ()
	print ("2 --> SPANISH")
	print ()
	print ("* --> Exit")
	print ()
	lang=input("")
	
	if lang == "1":
		mostra_instruccions("eng")
	elif lang == "2":
		mostra_instruccions("spa")
	else: 
		print ("Leaving...")
		os.system('sleep 2')
		
############## Menu #################

def menu():
	
	# Show a menu where user can select the actions
	# OUT: STR as the option he/she choosed
	
	os.system('clear')
	print ("Hello! You wanna play some BlackJack buddy?")
	print ()
	print ("1 --> Play")
	print ()
	print ("2 --> Rules")
	print ()
	print ("0 --> Exit")
	print ()
	opcio=input("")
	
	return opcio
	
###################### Bets #################


def es_numero(numero):
	
	# IN: String
	# OUT: Boolean
	
	if numero == '':
		return False
		
	for c in numero:
		
		if not ( c >= "0" and c <= "9"):
			return False
		
	return True
		
def aposta_invalida(aposta):
	
	# IN: String
	# OUT: String with a valid integer number
	
	while not es_numero(aposta):
		print ()
		print ("Not a valid bet. It has to be a integer")
		print ("Current stack:", saldo, "$")
		print ("USAGE: 50")
		print ("How much do you wanna bet?")
		print ()
		aposta=input("")
		
	return aposta
	
def aposta_minima(aposta):
	
	# IN: Integer lower than 10
	# OUT: Integer bigger than 10
	
	while aposta < 10:
		print ()
		print ("Not a valid bet. Minimum 10$")
		print ("Current stack:", saldo, "$")
		print ("USAGE: 50")
		print ("How much do you wanna bet?")
		print ()
		aposta=input("")
		if es_numero(aposta):
			aposta=int(aposta)
		else:
			aposta=0
		
	return aposta

def apostes(saldo):
	
	# IN: Integer that means the total stack
	# OUT: Integer that means the bet
	
	os.system('clear')
	print ("Current stack:", saldo, "$")
	print ("Minimum 10$. If you bet more than your stack, it'll be taken your stack as bet")
	print ("USAGE: 50")
	print ()
	print ("How much do you wanna bet?")
	aposta=input("")

	# If it's a number, assign integer type 
	if es_numero(aposta):
		aposta=int(aposta)
			
	# If not a number, repeat until it is
	else:
		aposta=aposta_invalida(aposta)
		aposta=int(aposta)
		
	# Minimum bet
	if aposta < 10:
		print (aposta_minima(aposta))
			
	if saldo < 10: 
		aposta = saldo
	# If user puts more than his stack
	if aposta > saldo:
		aposta = saldo
		
	return (aposta)
	
############## CARDS ###################

def generar_carta():
	
	# OUT: A tuple with the name of the card and the value.

	#Pikes	
	#Hearts
	#Clovers
	#Tiles

	cartes=[['Ace of \u2660',1],['2 of \u2660',2],['3 of \u2660',3],['4 of \u2660',4],['5 of \u2660',5],['6 of \u2660',6],['7 of \u2660',7],['8 of \u2660',8],['9 of \u2660',9],['10 of \u2660',10],['Jack of \u2660',10],['Queen of \u2660',10] ,['King of \u2660',10],['Ace of \u2665',1],['2 of \u2665',2],['3 of \u2665',3],['4 of \u2665',4],['5 of \u2665',5],['6 of \u2665',6],['7 of \u2665',7],['8 of \u2665',8],['9 of \u2665',9],['10 of \u2665',10],['Jack of \u2665',10],['Queen of \u2665',10] ,['King of \u2665',10],['Ace of \u2666',1],['2 of \u2666',2],['3 of \u2666',3],['4 of \u2666',4],['5 of \u2666',5],['6 of \u2666',6],['7 of \u2666',7],['8 of \u2666',8],['9 of \u2666',9],['10 of \u2666',10],['Jack of \u2666',10],['Queen of \u2666',10] ,['King of \u2666',10],['Ace of \u2663',1],['2 of \u2663',2],['3 of \u2663',3],['4 of \u2663',4],['5 of \u2663',5],['6 of \u2663',6],['7 of \u2663',7],['8 of \u2663',8],['9 of \u2663',9],['10 of \u2663',10],['Jack of \u2663',10],['Queen of \u2663',10] ,['King of \u2663',10]]
	
	opcio=randint(0,51) 
	
	carta=cartes[opcio]
	
	print (carta)
	
	

############## Programa ################

decisio=menu()

while (decisio != "0"):
	
	# Wanna play
	if (decisio == "1"):
		
		os.system('clear')
		print ("Welcome to GONER's CASINO, Good luck on beating our best crupier.")
		os.system('sleep 3')
		
		# User's turn
		print ("You will start with 100$, reach the 500$ and you'll win.")
		print ("Minimum bet 10$. If you bet more than your stack, it'll be taken your stack as bet")
		print ("If you got less than 10$ of stack, it'll be taken as the next bet automatically")
		saldo=100
		print ("If you lose all your money, you're out.")
		print ("Everything ok? ")
		sortir=input("Press a key to exit...")
		print ("Ok then, let's go!")
		os.system('sleep 3')
		
		aposta_final=apostes(saldo)
		print (aposta_final)
		
		# Generar cartes user
		
		# DOBLA / PLANTA / SEPARAR / +21
		
		# Generar cartes machine
		
		# Qui guanya, recalcular el saldo
		
		# Ha guanyat la partida? / Ha perdut		
		
		# Play and Exit
		decisio="0"
		
	# Wanna see the help
	elif (decisio == "2"):
		help_rules()
		decisio=menu()

	else: 
		print ("Not a valid char")
		sortida_help=input("Press any key to exit...")
		decisio=menu()

print ("See you soon b***")
