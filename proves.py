# !/usr/bin/python
# -*-coding: utf-8-*-

from random import randint
import os

############## CARDS ###################

def es_numero(numero):
	
	# IN: String
	# OUT: Boolean
	
	if numero == '':
		return False
		
	for c in numero:
		
		if not ( c >= "0" and c <= "9"):
			return False
		
	return True
		
def aposta_invalida(aposta):
	
	# IN: String
	# OUT: String with a valid integer number
	
	while not es_numero(aposta):
		print ()
		print ("Not a valid bet. It has to be a integer")
		print ("Current stack:", saldo, "$")
		print ("USAGE: 50")
		print ("How much do you wanna bet?")
		print ()
		aposta=input("")
		
	return aposta
	
def aposta_minima(aposta):
	
	# IN: Integer lower than 10
	# OUT: Integer bigger than 10
	
	while aposta < 10:
		print ()
		print ("Not a valid bet. Minimum 10$")
		print ("Current stack:", saldo, "$")
		print ("USAGE: 50")
		print ("How much do you wanna bet?")
		print ()
		aposta=input("")
		if es_numero(aposta):
			aposta=int(aposta)
		else:
			aposta=0
		
	return aposta

def generar_carta():
	
	# OUT: A tuple with the name of the card and the value.

	#Pikes	
	#Hearts
	#Clovers
	#Tiles

	cartes=[['Ace of \u2660',1],['2 of \u2660',2],['3 of \u2660',3],['4 of \u2660',4],['5 of \u2660',5],['6 of \u2660',6],['7 of \u2660',7],['8 of \u2660',8],['9 of \u2660',9],['10 of \u2660',10],['Jack of \u2660',10],['Queen of \u2660',10] ,['King of \u2660',10],['Ace of \u2665',1],['2 of \u2665',2],['3 of \u2665',3],['4 of \u2665',4],['5 of \u2665',5],['6 of \u2665',6],['7 of \u2665',7],['8 of \u2665',8],['9 of \u2665',9],['10 of \u2665',10],['Jack of \u2665',10],['Queen of \u2665',10] ,['King of \u2665',10],['Ace of \u2666',1],['2 of \u2666',2],['3 of \u2666',3],['4 of \u2666',4],['5 of \u2666',5],['6 of \u2666',6],['7 of \u2666',7],['8 of \u2666',8],['9 of \u2666',9],['10 of \u2666',10],['Jack of \u2666',10],['Queen of \u2666',10] ,['King of \u2666',10],['Ace of \u2663',1],['2 of \u2663',2],['3 of \u2663',3],['4 of \u2663',4],['5 of \u2663',5],['6 of \u2663',6],['7 of \u2663',7],['8 of \u2663',8],['9 of \u2663',9],['10 of \u2663',10],['Jack of \u2663',10],['Queen of \u2663',10] ,['King of \u2663',10]]
	
	opcio=randint(0,51) 
	
	carta=cartes[opcio]
	
	if carta[1] == 1:
		print ('Your card is an Ace, which value do you want? 1 or 11?')
		ace=input('')
		# Si posa 1 o 11, el canviem, si no l'hem de tornar a preguntar
		
		while ace != 1 or ace != 11:
			
			if es_numero(ace):
				ace=int(ace)
			
			
		
		
		
		
		
		
		
	
	print (carta)
	

generar_carta()

#print (u'\1f0a1')

#U+1F0A1

#U+1F0A1

#!/usr/bin/python
myString = '4 \u2660'
print(myString)


