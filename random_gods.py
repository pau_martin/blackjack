# /usr/bin/python3
#-*- coding: utf-8-*-

#### Randomizer de deus d'Smite ####
import random

# Llista de déus

gods=["Achilles", "Agni", "Ah Muzen Cab", "Ah Puch", "Amaterasu", "Anhur", "Anubis", "Ao Kuang", "Aphrodite", "Apollo", "Arachne", "Ares", "Artemis", "Artio", "Athena", "Awilix", "Baba Yaga", "Bacchus", "Bakasura", "Baron Samedi", \
"Bastet", "Bellona", "Cabrakan", "Camazotz", "Cerberus", "Cernunnos", "Chaac", "Chang'e", "Charybdis", "Chernobog", "Chiron", "Chronos", "Cthulhu", "Cu Chulainn", "Cupid", "Da Ji", "Danzaburou", "Discordia", "Erlang Shen", \
"Eset", "Fafnir", "Fenrir", "Freya", "Ganesha", "Geb", "Gilgamesh", "Guan Yu", "Hachiman", "Hades", "He Bo", "Heimdallr", "Hel", "Hera", "Hercules", "Horus", "Hou Yi", "Hun Batz", "Izanami", "Janus", "Jing Wei", "Jormungandr", \
"Kali", "Khepri", "King Arthur", "Kukulkan", "Kumbhakarna", "Kuzenbo", "Loki", "Medusa", "Mercury", "Merlin", "Morgan Le Mis Huevos", 	"Mulan", "Ne Zha", "Neith", "Nemesis", "Nike", "Nox", "Nu Wa", "Odin", "Olorun", "Osiris", \
"Pele", "Persephone", "Poseidon", "Ra", "Raijin", "Rama", "Ratatoskr", "Ravana", "Scylla", "Serqet", "Set", "Skadi", "Sobek", "Sol", "Sun Wukong", "Susano", "Sylvanus", "Terra", "Thanatos", "The Morrigan", "Thor", "Thoth", \
"Tiamat", "Tsukuyomi", "Tyr", "Ullr", "Vamana", "Vulcan", "Xbalanque", "Xing Tian", "Yemoja", "Ymir", "Zeus", "Zhong Kui"]

# Barrejem la llista de deus.
random.shuffle(gods)

# Agafem números random per a cada un
num_carles=random.randint(0,len(gods))
num_max=random.randint(0,len(gods))
num_pau=random.randint(0,len(gods))

# Agafem el déu random de cada un
deu_carles=gods[num_carles]
deu_max=gods[num_max]
deu_pau= gods[num_pau]


# Mostrem
print ()
print ("Carles: ", deu_carles)
print ("Max:    ", deu_max)
print ("Pau:    ", deu_pau)
print ()
